from djplus.models import (
    ActivatableModel,
    ActivatableQuerySet,
    CancellableModel,
    CancellableQuerySet,
    CancellationModel,
    CancellationQuerySet,
    LockableModel,
    LockableQuerySet,
    TimestampableModel,
    UserstampableModel,
    UserstampableQuerySet,
)


class NewActivatableModel(ActivatableModel):
    objects = ActivatableQuerySet.as_manager()


class NewCancellableModel(CancellableModel):
    objects = CancellableQuerySet.as_manager()


class NewCancellationModel(CancellationModel):
    objects = CancellationQuerySet.as_manager()


class NewLockableModel(LockableModel):
    objects = LockableQuerySet.as_manager()


class NewTimestampableModel(TimestampableModel):
    pass


class NewUserstampableModel(UserstampableModel):
    objects = UserstampableQuerySet.as_manager()
