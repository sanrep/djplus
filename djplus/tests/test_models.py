import time

from django.contrib.auth import get_user_model
from django.utils import timezone

import pytest

from .models import (
    NewActivatableModel,
    NewCancellableModel,
    NewCancellationModel,
    NewLockableModel,
    NewTimestampableModel,
    NewUserstampableModel,
)

pytestmark = pytest.mark.django_db


# ----------------------------------------------------------------------
def test_activatable_queryset():
    new_instance = NewActivatableModel.objects.create()
    q = NewActivatableModel.objects.active()
    assert q.count() == 1
    q = NewActivatableModel.objects.inactive()
    assert q.count() == 0

    new_instance.is_active = False
    new_instance.save()
    q = NewActivatableModel.objects.inactive()
    assert q.count() == 1
    q = NewActivatableModel.objects.active()
    assert q.count() == 0


# ----------------------------------------------------------------------
def test_cancellable_queryset():
    new_instance = NewCancellableModel.objects.create()
    q = NewCancellableModel.objects.not_cancelled()
    assert q.count() == 1
    q = NewCancellableModel.objects.cancelled()
    assert q.count() == 0

    new_instance.is_cancelled = True
    new_instance.save()
    q = NewCancellableModel.objects.cancelled()
    assert q.count() == 1
    q = NewCancellableModel.objects.not_cancelled()
    assert q.count() == 0


# ----------------------------------------------------------------------
def test_cancellation_queryset():
    usermodel = get_user_model()
    new_user = usermodel.objects.create()
    new_instance = NewCancellationModel.objects.create()
    q = NewCancellationModel.objects.cancelled_by(new_user)
    assert q.count() == 0
    q = NewCancellationModel.objects.not_cancelled()
    assert q.count() == 1
    q = NewCancellationModel.objects.cancelled()
    assert q.count() == 0

    new_instance.cancelled_by = new_user
    new_instance.cancelled_at = timezone.now()
    new_instance.save()
    q = NewCancellationModel.objects.cancelled_by(new_user)
    assert q.count() == 1
    q = NewCancellationModel.objects.cancelled_by(new_user.id)
    assert q.count() == 1
    q = NewCancellationModel.objects.not_cancelled()
    assert q.count() == 0
    q = NewCancellationModel.objects.cancelled()
    assert q.count() == 1


# ----------------------------------------------------------------------
def test_lockable_queryset():
    new_instance = NewLockableModel.objects.create()
    q = NewLockableModel.objects.unlocked()
    assert q.count() == 1
    q = NewLockableModel.objects.locked()
    assert q.count() == 0

    new_instance.is_locked = True
    new_instance.save()
    q = NewLockableModel.objects.locked()
    assert q.count() == 1
    q = NewLockableModel.objects.unlocked()
    assert q.count() == 0


# ----------------------------------------------------------------------
def test_timestampable_model():
    new_instance = NewTimestampableModel()
    assert new_instance.created_at is None
    assert new_instance.updated_at is None

    new_instance.save()
    now = timezone.now()
    assert new_instance.created_at < now
    assert new_instance.updated_at < now

    created_at = new_instance.created_at
    time.sleep(1)
    new_instance.save()
    assert new_instance.created_at == created_at
    assert new_instance.updated_at > created_at


# ----------------------------------------------------------------------
def test_userstampable_queryset():
    usermodel = get_user_model()
    new_user = usermodel.objects.create()
    new_instance = NewUserstampableModel()
    q = NewUserstampableModel.objects.created_by(new_user)
    assert q.count() == 0
    q = NewUserstampableModel.objects.updated_by(new_user)
    assert q.count() == 0

    new_instance.save()
    q = NewUserstampableModel.objects.created_by(new_user)
    assert q.count() == 1
    q = NewUserstampableModel.objects.created_by(new_user.id)
    assert q.count() == 1
    q = NewUserstampableModel.objects.updated_by(new_user)
    assert q.count() == 1
