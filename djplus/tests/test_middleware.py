import threading
import time
from unittest.mock import Mock

from django.contrib.auth import get_user_model

import pytest

from djplus.middleware import CurrentUserMiddleware

pytestmark = pytest.mark.django_db


class TestCurrentUserMiddleware:
    def test_current_user_is_none_if_no_user_given(self):
        assert CurrentUserMiddleware.get_current_user() is None

    def test_current_user_is_none_if_request_user_is_none(self):
        set_current_user(user=None)
        assert CurrentUserMiddleware.get_current_user() is None

    def test_current_user_is_same_as_request_user(self):
        usermodel = get_user_model()
        user1 = usermodel.objects.create(username='username1')
        set_current_user(user=user1)
        current_user = CurrentUserMiddleware.get_current_user()

        assert current_user is not None
        assert current_user == user1
        assert current_user.username == 'username1'

    def test_current_user_is_thread_safe(self):
        usermodel = get_user_model()
        user1 = usermodel.objects.create(username='username1')
        user2 = usermodel.objects.create(username='username2')
        current_users = []

        first_thread = threading.Thread(target=set_current_user, args=(user1, 0, 5, current_users))
        second_thread = threading.Thread(target=set_current_user, args=(user2, 3, 0, current_users))
        first_thread.start()
        second_thread.start()
        first_thread.join()
        second_thread.join()

        assert current_users[0] == user2
        assert current_users[0].username == 'username2'
        assert current_users[1] == user1
        assert current_users[1].username == 'username1'


def set_current_user(user=None, delay_before_request=0, delay_after_request=0, current_users=None):
    request = Mock()
    request.user = user
    middleware = CurrentUserMiddleware()

    if delay_before_request:
        time.sleep(delay_before_request)
    middleware.process_request(request)
    if delay_after_request > 0:
        time.sleep(delay_after_request)

    if current_users is not None:
        current_users.append(CurrentUserMiddleware.get_current_user())
