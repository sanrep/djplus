"""Middleware for current user. Taken from ai-django-core."""

from threading import local

_user = local()


class CurrentUserMiddleware(object):
    """Middleware which stores request's user into global thread-safe
    variable.

    Must be introduced after
    `django.contrib.auth.middleware.AuthenticationMiddleware`.
    """
    def process_request(self, request):
        _user.value = request.user

    @staticmethod
    def get_current_user():
        return _user.value if hasattr(_user, 'value') else None
