from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .middleware import CurrentUserMiddleware


# ----------------------------------------------------------------------
class ActivatableQuerySet(models.QuerySet):
    """ Queryset methods for filtering active/inactive instances.

    Should be used as a mixin for querysets that describe model which
    has ActivatableModel as parent.

    E.g.:
      Define child queryset:
          class ChildQuerySet(ActivatableQuerySet):

      In child model class define:
          objects = ChildQuerySet.as_manager()
    """
    def active(self):
        return self.filter(is_active=True)

    def inactive(self):
        return self.filter(is_active=False)


# ----------------------------------------------------------------------
class ActivatableModel(models.Model):
    """ An abstract base class model which provides activating instances.

    By default instance is active.
    """
    is_active = models.BooleanField(
        default=True,
        verbose_name=_('is active'),
    )

    class Meta:
        abstract = True


# ----------------------------------------------------------------------
class CancellableQuerySet(models.QuerySet):
    """ Queryset methods for filtering cancelled/not cancelled instances.

    Should be used as a mixin for querysets that describe model which
    has CancellableModel as parent.

    E.g.:
      Define child queryset:
          class ChildQuerySet(CancellableQuerySet)

      In child model class define:
          objects = ChildQuerySet.as_manager()
    """
    def cancelled(self):
        return self.filter(is_cancelled=True)

    def not_cancelled(self):
        return self.filter(is_cancelled=False)


# ----------------------------------------------------------------------
class CancellableModel(models.Model):
    """ An abstract base class model which provides cancelling instances.

    By default instance is not cancelled.
    """
    is_cancelled = models.BooleanField(
        default=False,
        verbose_name=_('is cancelled'),
    )

    class Meta:
        abstract = True


# ----------------------------------------------------------------------
class CancellationQuerySet(models.QuerySet):
    """ Queryset methods for filtering instances cancelled by a user.

    Should be used as a mixin for querysets that describe model which
    has CancellationModel as parent.

    E.g.:
      Define child queryset:
          class ChildQuerySet(CancelledQuerySet)

      In child model class define:
          objects = ChildQuerySet.as_manager()
    """
    def cancelled(self):
        return self.filter(cancelled_at__isnull=False)

    def not_cancelled(self):
        return self.filter(cancelled_at__isnull=True)

    def cancelled_by(self, user):
        """Filtering instances cancelled by user.

        :user: instance of settings.AUTH_USER_MODEL or user id
        """
        return self.filter(cancelled_by=user)


# ----------------------------------------------------------------------
class CancellationModel(models.Model):
    """ An abstract base class model which provides attributes for
    cancellation of instances.
    """
    cancelled_at = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_('cancelled at'),
    )
    cancelled_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='%(app_label)s_%(class)s_cancelled_by',
        verbose_name=_('cancelled by'),
    )
    cancellation_reason = models.TextField(
        blank=True,
        verbose_name=_('cancellation reason'),
    )

    class Meta:
        abstract = True


# ----------------------------------------------------------------------
class LockableQuerySet(models.QuerySet):
    """ Queryset methods for filtering locked/unlocked instances.

    Should be used as a mixin for querysets that describe model which
    has LockableModel as parent.

    E.g.:
      Define child queryset:
          class ChildQuerySet(LockableQuerySet)

      In child model class define:
          objects = ChildQuerySet.as_manager()
    """
    def locked(self):
        return self.filter(is_locked=True)

    def unlocked(self):
        return self.filter(is_locked=False)


# ----------------------------------------------------------------------
class LockableModel(models.Model):
    """ An abstract base class model which provides locking instances.

    By default instance is unlocked.
    """
    is_locked = models.BooleanField(
        default=False,
        verbose_name=_('is locked'),
    )

    class Meta:
        abstract = True


# ----------------------------------------------------------------------
class TimestampableModel(models.Model):
    """ An abstract base class model which provides timestamps fields.

    Fields created_at and updated_at are meant to be used for storing
    a timestamp when the model instance was created and last updated.

    Time-stamps are populated using pre_save signal timestampable.
    Unfortunately, creating and updating objects in bulk doesn't
    update created_by and modified_by fields, since pre_save is not
    called.
    """
    created_at = models.DateTimeField(
        null=True,
        blank=True,
        editable=False,
        auto_now_add=True,
        verbose_name=_('created at'),
    )
    updated_at = models.DateTimeField(
        null=True,
        blank=True,
        editable=False,
        auto_now=True,
        verbose_name=_('updated at'),
    )

    class Meta:
        abstract = True
        get_latest_by = 'updated_at'


# ----------------------------------------------------------------------
class UserstampableQuerySet(models.QuerySet):
    """ Queryset methods for filtering instances created by a user.

    Should be used as a mixin for querysets that describe model which
    has UserstampableModel as parent.

    E.g.:
      Define child queryset:
          class ChildQuerySet(UserstampableQuerySet)

      In child model class define:
          objects = ChildQuerySet.as_manager()
    """
    def created_by(self, user):
        """Filtering instances created by user.

        :user: Instance of settings.AUTH_USER_MODEL or user id
        """
        return self.filter(created_by=user)

    def updated_by(self, user):
        """Filtering instances updated by user.

        :user: Instance of settings.AUTH_USER_MODEL or user id
        """
        return self.filter(updated_by=user)


# ----------------------------------------------------------------------
class UserstampableModel(models.Model):
    """ An abstract base class model which provides userstamp fields.

     Fields created_by and updated_by are meant to be used for storing
     a user which creates and last updates the model instance.

    Deletion of the referenced created_by and modified_by objects is
    protected.

    `djplus.middleware.CurrentUserMiddleware` must be added to
    settings.MIDDLEWARE after
    `django.contrib.auth.middleware.AuthenticationMiddleware`.
    """
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='%(app_label)s_%(class)s_created_by',
        verbose_name=_('created by'),
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='%(app_label)s_%(class)s_updated_by',
        verbose_name=_('updated by'),
    )

    class Meta:
        abstract = True

    @staticmethod
    def get_current_user():
        """Get current user from CurrentUserMiddleware.

        Can be overwritten to use other middleware or additional
        functionality

        :return: Instance of settings.AUTH_USER_MODEL
        """
        return CurrentUserMiddleware.get_current_user()

    def save(self, *args, **kwargs):
        current_user = self.get_current_user()
        if not self.pk:
            self.created_by = current_user
        self.updated_by = current_user
        super().save(*args, **kwargs)
