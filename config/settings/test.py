"""Testing settings"""

from .base import *  # noqa: F403, F401

SECRET_KEY = 'fake-key'

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        "NAME": ":memory:",
    }
}
